package com.harriague.automate.module.android.osnative.utils;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class fileHelper {

    /**
     * Logger object
     */
    private static Logger log = Logger
            .getLogger(ImagesComparison.class.getName());

    /**
     * Servers parameters
     */
    private static final int BUFFER_SIZE = 4096;

    /**
     * Downloads a file from a FTP server
     *
     * @param resourceDir    destiny folder
     * @param firstDirChild  first directory child of remote path
     * @param secondDirChild second directory child of remote path
     * @param fileName       file name
     * @return downloaded file path
     */
    public String getFileFTP(String serverIP, int serverPort, String user, String password, String remotePath, String
            resourceDir, String firstDirChild, String secondDirChild, String fileName) throws IOException {

        String downloadedImagePath = resourceDir + secondDirChild + "-" + fileName;

        File downloadedFile = new File(downloadedImagePath);

        FTPClient ftpClient = new FTPClient();
        try {

            ftpClient.connect(serverIP, serverPort);
            ftpClient.login(user, password);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            String remoteFile1 = remotePath + firstDirChild + File.separator + secondDirChild + File.separator +
                    fileName;

            downloadedFile.createNewFile();

            OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadedFile));
            boolean success = ftpClient.retrieveFile(remoteFile1, outputStream1);
            outputStream1.close();

            if (success) {
                log.info("File " + fileName + " has been downloaded successfully.");
            } else {
                throw new IOException("Retrieve Error, file " + fileName + " not downloaded.");
            }

        } catch (IOException ex) {

            throw ex;

        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                log.info("Error closing FTP connection");
            }
        }
        return downloadedImagePath;
    }

    /**
     * Downloads a file from a URL
     *
     * @param fileURL       HTTP URL of the server that host the tree folder
     * @param resourceDir   destiny folder
     * @param specVersion   HMI Spec Version
     * @param appName       App Name
     * @param screenHMIName screen spec name
     * @return downloaded File object
     * @throws IOException
     */
    public String getFileHTTP(String resourceDir, String fileURL, String specVersion, String appName, String
            screenHMIName) throws
            IOException {
        URL url = new URL(fileURL + specVersion + appName + screenHMIName);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        int responseCode = httpConn.getResponseCode();

        String downloadImagePath = resourceDir + appName + "-" + screenHMIName;

        File downloadedFile = new File(downloadImagePath);

        // always check HTTP response code first
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();

            if (disposition != null) {
                // extracts file name from header field
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10,
                            disposition.length() - 1);
                }
            } else {
                // extracts file name from URL
                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                        fileURL.length());
            }

            log.info("fileName = " + fileName);

            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();

            // opens an output stream to save into file

            downloadedFile.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(downloadImagePath);

            int bytesRead = -1;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            outputStream.close();
            inputStream.close();

            log.info("File downloaded from HTTP server");
        } else {
            log.info("No file to download. Server replied HTTP code: " + responseCode);
        }
        httpConn.disconnect();

        return downloadImagePath;
    }

}
