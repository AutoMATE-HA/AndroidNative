package com.harriague.automate.module.android.osnative.conf;

import com.harriague.automate.core.exceptions.AgentException;
import com.harriague.automate.core.exceptions.PropertyException;
import com.harriague.automate.core.utils.ReadProperty;

import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.Map;

public class AVDManager {

    private final static String SEPARATOR = java.io.File.separator;
    private static final String ANDROID_HOME = System.getenv("ANDROID_HOME");
    private static final String GET_DEVICES_COMMAND_LINE = ANDROID_HOME + SEPARATOR + "platform-tools" + SEPARATOR +
            "adb devices";
    private static final String DEVICE_READY = "device";
    private static final String START_EMULATOR_COMMAND_LINE = ANDROID_HOME + SEPARATOR + "tools" + SEPARATOR +
            "emulator -avd <avd_name> -no-window";
    private static final String GET_EMULATORS_COMMAND_LINE = ANDROID_HOME + SEPARATOR + "tools" + SEPARATOR +
            "emulator -list-avds";
    private static final String CHECK_ANIM_COMMAND_LINE = ANDROID_HOME + SEPARATOR + "platform-tools" + SEPARATOR +
            "adb shell getprop init.svc.bootanim";
    private static final String PROXY_COMMAND_LINE = " -http-proxy ";
    private static final String KILL_EMULATOR_X86_COMMAND_LINE = SystemUtils.IS_OS_WINDOWS ? "taskkill /F /IM " +
            "emulator.exe" : "pkill -f emulator-x86";
    private static final String KILL_EMULATOR_ARM_COMMAND_LINE = SystemUtils.IS_OS_WINDOWS ? "taskkill /F /IM " +
            "qemu-system-i386.exe" : "pkill -f emulator-arm";

    private static Logger log = Logger.getLogger("AVDManager");

    private static Hashtable<String, Boolean> AVDS = new Hashtable<>();

    public static String startRandomEmulator() throws IOException, AgentException {
        String name = getUnusedAdv(true);
        startEmulator(name);
        return name;
    }

    public static String getUnusedAdv(boolean markAsUsed) throws IOException {
        if(AVDS.size() == 0) {
            setEmulators();
        }
        for (Map.Entry<String, Boolean> avd : AVDS.entrySet()) {
            if (!avd.getValue()) {
                if (markAsUsed) {
                    avd.setValue(true);
                }
                return avd.getKey();
            }
        }
        return null;
    }

    public static void setEmulators() throws IOException {
        log.info("Getting a list of emulators");
        AVDS = new Hashtable<>();
        Process p = Runtime.getRuntime().exec(GET_EMULATORS_COMMAND_LINE);
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            AVDS.put(line,false);
        }
        log.info(AVDS.size() +" emulators found");
    }

    public static void startEmulator(String deviceName) throws AgentException {
        startAnEmulator(deviceName);
    }

    public static void killEmulator() throws IOException, InterruptedException {
        log.info("About to kill emulators.");
        Process p = Runtime.getRuntime().exec(KILL_EMULATOR_X86_COMMAND_LINE);
        p.waitFor();
        p = Runtime.getRuntime().exec(KILL_EMULATOR_ARM_COMMAND_LINE);
        p.waitFor();
    }

    private static void startAnEmulator(String deviceName) throws AgentException {
        log.info("Starting emulator");
        StringBuilder command = new StringBuilder();
        command.append(START_EMULATOR_COMMAND_LINE.replace("<avd_name>", deviceName));
        String proxy = ReadProperty.getProperty(PropertiesKeys.ANDROID_EMULATOR_PROXY_TEMPLATE
                + ReadProperty.environment, null);
        if(proxy != null) {
            log.info("Seting proxy '"+proxy+"' in "+deviceName);
            command.append(PROXY_COMMAND_LINE);
            command.append(proxy);
        }
        try {
            Runtime.getRuntime().exec(command.toString());
            log.info("Emulator started. Waiting to be ready");
            waitForAnimationEnd();
        } catch (Exception e) {
            throw new AgentException(e.getMessage(), e, null);
        }
    }

    private static void waitForAnimationEnd() throws IOException, InterruptedException, PropertyException {
        // Checks android properties for animation state
        Process p = Runtime.getRuntime().exec(CHECK_ANIM_COMMAND_LINE);
        int tries = 0;
        BufferedReader animation = new BufferedReader(new InputStreamReader(
                p.getInputStream()));

        Thread.sleep(ReadProperty.getPropertyInt(
                PropertiesKeys.ANDROID_WAIT_FOR_READY, Constants.ANDROID_EMULATOR_WAIT_FOR_READY) * 1000);

        while (!animation.readLine().contains("stopped")) {
            Thread.sleep(5000);
            p = Runtime.getRuntime().exec(CHECK_ANIM_COMMAND_LINE);
            animation = new BufferedReader(new InputStreamReader(p.getInputStream()));
            if (tries++ > 6)
                throw new InterruptedException("AVDManager.waitForAnimationEnd timeout");
        }
    }
}
