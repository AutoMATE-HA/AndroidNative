package com.harriague.automate.module.android.osnative.utils;

import com.harriague.automate.core.structures.SwipeDirection;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.bytedeco.javacpp.DoublePointer;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Point;
import org.bytedeco.javacpp.opencv_core.Size;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;

import javax.imageio.ImageIO;

import static org.bytedeco.javacpp.opencv_core.CV_32FC1;
import static org.bytedeco.javacpp.opencv_core.inRange;
import static org.bytedeco.javacpp.opencv_core.minMaxLoc;
import static org.bytedeco.javacpp.opencv_core.rotate;
import static org.bytedeco.javacpp.opencv_core.subtract;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.COLOR_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.COLOR_BGR2HSV;
import static org.bytedeco.javacpp.opencv_imgproc.MORPH_RECT;
import static org.bytedeco.javacpp.opencv_imgproc.TM_CCORR_NORMED;
import static org.bytedeco.javacpp.opencv_imgproc.blur;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.dilate;
import static org.bytedeco.javacpp.opencv_imgproc.getStructuringElement;
import static org.bytedeco.javacpp.opencv_imgproc.matchTemplate;

public class ImagesComparison {


    /**
     * OpenCv parameters
     */
    private static final double MAXIMUM_MATCH_SCORE = 0.9911;
    private static final double MINIMUM_MATCH_SCORE = 0.8;
    private static final double BLUR_COEFFICIENT = 2.0;
    private static final String PNG = "png";
    /**
     * RGBA Parameters
     */
    private static final int ALPHA = 24;
    private static final int RED = 16;
    private static final int GREEN = 8;
    private static final int BLUE = 0;
    /**
     * Logger object
     */
    private static Logger log = Logger.getLogger(ImagesComparison.class.getName());

    /**
     * Subtract the input file to the template a file
     *
     * @param destinationFolder folder where the resulting image will be saved
     * @param templateFilePath  full path of the template file
     * @param inFilePath        full path of the input file
     * @return resulting image path
     */
    public static String subtractImages(String destinationFolder, String templateFilePath, String inFilePath) {

        String resultImage = destinationFolder + "SubtractResult" + FilenameUtils.getName(templateFilePath);

        Mat templateImage = readImage(templateFilePath);
        Mat toCompareImage = readImage(inFilePath);

        Mat diff = new Mat();
        subtract(templateImage, toCompareImage, diff);

        writeMatImage(diff, resultImage);

        return resultImage;
    }

    /**
     * @param templateFilePath full path of the template file
     * @param inFilePath       full path of the input file
     * @param rectangles       dimensions of the rectangle to draw
     * @throws IOException
     */
    public static void drawRectanglesOnScreens(String templateFilePath, String inFilePath, List<Rectangle> rectangles)
            throws IOException {

        BufferedImage inImg = readBufferedImage(inFilePath);
        BufferedImage templateImg = readBufferedImage(templateFilePath);

        for (Rectangle rect : rectangles) {
            drawRectanglesOnImage(inImg, rect);
            drawRectanglesOnImage(templateImg, rect);
        }

        writeBufferedImage(inFilePath, inImg);
        writeBufferedImage(templateFilePath, templateImg);

    }

    /**
     * @param templateFilePath full path of the template file
     * @param inFilePath       full path of the input file
     * @param rectangle        rectangle element to draw
     * @throws IOException
     */
    public static void drawRectanglesOnScreens(String templateFilePath, String inFilePath, Rectangle rectangle)
            throws IOException {

        List<Rectangle> rectangles = new ArrayList<Rectangle>() {{
            add(rectangle);
        }};

        drawRectanglesOnScreens(templateFilePath, inFilePath, rectangles);
    }

    /**
     * Return in percentage the differences pixel by pixel between two images, and create an new image showing where the
     * differences are.
     *
     * @param inFilePath       full path of the input file
     * @param templateFilePath full path of the template file
     * @return differences percentage
     */
    public static int calculateDifferences(String inFilePath, String templateFilePath, String destinationFolder) throws
            InputMismatchException,
            IOException {

        BufferedImage inImg = readBufferedImage(inFilePath);
        int widthInImg = inImg.getWidth();
        int heightInImg = inImg.getHeight();

        long diffAcum = countDifference(inFilePath, templateFilePath, destinationFolder);

        double n = widthInImg * heightInImg;
        double p = diffAcum / n;

        int percentDifference = (int) (p * 100.0);

        log.info("Difference percent: " + percentDifference + "%");
        log.info("Failed Pixels:" + diffAcum + " over " + (int) n + " " + "total");

        return percentDifference;
    }


    /**
     * Return in percentage the differences pixel by pixel between two images, and create an new image showing where the
     * differences are.
     *
     * @param inFilePath       full path of the input file
     * @param templateFilePath full path of the template file
     * @return differences percentage
     */
    public static int calculateDifferencesBetweenPoints(String inFilePath, String templateFilePath, String destinationFolder, java.awt.Point start, java.awt.Point end) throws
            InputMismatchException,
            IOException {

        long diffAcum = countDifference(inFilePath, templateFilePath, destinationFolder);

        double n = (end.x - start.x) * (end.y - start.y);
        double p = diffAcum / n;

        int percentDifference = (int) (p * 100.0);

        log.info("Difference percent: " + percentDifference + "%");
        log.info("Failed Pixels:" + diffAcum + " over " + (int) n + " " + "total");

        return percentDifference;
    }

    private static long countDifference(String inFilePath, String templateFilePath, String destinationFolder) throws IOException {
        File inFile = new File(inFilePath);
        File templateFile = new File(templateFilePath);
        BufferedImage inImg = readBufferedImage(inFilePath);
        BufferedImage templateImg = readBufferedImage(templateFilePath);

        int widthInImg = inImg.getWidth();
        int heightInImg = inImg.getHeight();

        int widthTemplateImg = templateImg.getWidth();
        int heightTemplateImg = templateImg.getHeight();

        BufferedImage outImg = new BufferedImage(widthInImg, heightInImg, 1);

        if ((widthInImg != widthTemplateImg) || (heightInImg != heightTemplateImg)) {
            throw new InputMismatchException("Error: Images dimensions mismatch");
        }

        long diffAcum = 0;
        for (int y = 0; y < heightInImg; y++) {
            for (int x = 0; x < widthInImg; x++) {
                int[] rgb1 = getRGBA(inImg, x, y);
                int[] rgb2 = getRGBA(templateImg, x, y);
                if (!Arrays.equals(rgb1, rgb2)) {
                    ++diffAcum;
                    outImg.setRGB(x, y, Color.red.getRGB());
                }
            }
        }

        String resultImage = destinationFolder + "ComparisionResult" + FilenameUtils.getName(templateFilePath);
        writeBufferedImage(resultImage, outImg);

        deleteFile(inFile);
        deleteFile(templateFile);

        return diffAcum;
    }

    /**
     * Tries to find a match between the images and returns coordinates of the
     * mach
     *
     * @param inFile
     * @param templateFile
     * @param lowPrecision
     * @return
     */
    public static java.awt.Point matchImages(String inFile, String templateFile, boolean lowPrecision) {

        log.info("matching images");
        java.awt.Point coord = new java.awt.Point();

        int matchMethod = TM_CCORR_NORMED;

        Mat screenShot = imread(inFile);
        Mat template = imread(templateFile);

        // Create the result matrix
        int resultCols = screenShot.cols() - template.cols() + 1;
        int resultRows = screenShot.rows() - template.rows() + 1;
        Mat result = new Mat(resultRows, resultCols, CV_32FC1);

        // Match Template Function from OpenCV
        matchTemplate(screenShot, template, result, matchMethod);

        DoublePointer min_val = new DoublePointer();
        DoublePointer max_val = new DoublePointer();

        Point minLoc = new Point();
        Point maxLoc = new Point();

        minMaxLoc(result, min_val, max_val, minLoc, maxLoc, null);

        Point matchLoc;
        matchLoc = maxLoc;

        log.info("Image matched a score of: " + max_val);

        if (max_val.get() >= MAXIMUM_MATCH_SCORE || (lowPrecision && max_val.get() >= MINIMUM_MATCH_SCORE)) {
            if (matchLoc.x() == 0 && matchLoc.y() == 0) {
                coord.setLocation(1, 1);
            } else {
                coord.setLocation(matchLoc.x(), matchLoc.y());
            }
        } else {
            log.info("no matches");
        }
        return coord;
    }

    /**
     * Blurs an image
     *
     * @param input         source image matrix
     * @param numberOfTimes times to apply blurImage filter
     * @return
     */
    public static Mat blurImage(Mat input, int numberOfTimes) {
        Mat sourceImage;
        Mat destinationImage = input.clone();

        log.info("applying blurImage...");

        for (int i = 0; i < numberOfTimes; i++) {
            sourceImage = destinationImage.clone();
            blur(sourceImage, destinationImage, new Size((int) BLUR_COEFFICIENT, (int) BLUR_COEFFICIENT));
        }
        return destinationImage;
    }

    public static Mat dilateImage(Mat input) {
        Mat dest = new Mat();
        Mat element1 = getStructuringElement(MORPH_RECT, new Size(10, 10));

        dilate(input, dest, element1);

        return dest;
    }

    /**
     * Converts an image to gray scale
     *
     * @param input
     * @return
     */
    public static Mat convertMatToGray(Mat input) {

        Mat destinationImage = input.clone();
        log.info("converting to gray...");
        cvtColor(destinationImage, destinationImage, COLOR_BGR2GRAY);

        return destinationImage;
    }

    /**
     * Converts an image to color space passed as parameter
     *
     * @param input
     * @return
     */
    public static Mat convertMatToColorSpace(Mat input) {

        Mat destinationImage = input.clone();
        log.info("converting to other color space " + "COLOR_BGR2HSV");
        cvtColor(destinationImage, destinationImage, COLOR_BGR2HSV);

        return destinationImage;
    }

    /**
     * Isolate part of the image passed from screenshot by color range
     *
     * @param screenshotInput image
     * @return image with car icon only
     */
    public static Mat obtainPartByColor(String screenshotInput, Mat lowRange, Mat highRange) {

        Mat hsv = new Mat();

        Mat screenShot = imread(screenshotInput);

        cvtColor(screenShot, hsv, COLOR_BGR2HSV);


        inRange(screenShot, lowRange, highRange, hsv);

        return dilateImage(hsv);
    }

    /**
     * Rotates an image 90 degrees
     *
     * @param image
     * @param angle 0 = Rotate 90 degrees clockwise; 1 = Rotate 180 degrees clockwise; 2 = Rotate 270 degrees clockwise
     * @return
     */
    public static Mat rotateImage(Mat image, int angle) {
        Mat destinationImage = new Mat();

        destinationImage.create(image.size(), image.type());

        rotate(destinationImage, destinationImage, angle);

        return destinationImage;
    }

    /**
     * Reads an image, receives image name as: image_directory + image_name
     *
     * @param imageFile
     * @return
     */
    public static Mat readImage(String imageFile) {
        Mat image = imread(imageFile);
        return image;
    }

    /**
     * Reads an image from a buffer
     *
     * @param inputFilePath
     */
    public static BufferedImage readBufferedImage(String inputFilePath) throws IOException {
        File inputFile = new File(inputFilePath);
        BufferedImage bufferedImage;

        try {
            bufferedImage = ImageIO.read(inputFile);
        } catch (IOException e) {
            throw new IOException("Error reading the screenshot file: " + inputFile);
        }
        return bufferedImage;
    }

    /**
     * Writes an image from a buffer
     *
     * @param outputFilePath
     * @param bufferedImage
     */
    public static BufferedImage writeBufferedImage(String outputFilePath, BufferedImage bufferedImage) {
        File outputFile = new File(outputFilePath);

        try {
            ImageIO.write(bufferedImage, PNG, outputFile);
            return bufferedImage;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bufferedImage;
    }

    /**
     * Writes an image from a matrix
     *
     * @param image
     * @param outFileName
     */
    public static void writeMatImage(Mat image, String outFileName) {
        imwrite(outFileName, image);
    }

    /**
     * Returns a region of interest (ROI) from the image matrix
     *
     * @param image
     * @param direction
     * @return
     */
    public static Mat getPortion(Mat image, SwipeDirection direction) throws IllegalArgumentException {
        // TODO it is not the optimal, the number were getting manually to
        // identify a specific fragment of the image in the EVM.
        // A better solution is needed for this function.
        // 8 represents amount of sections we divide the screen on
        int xPortion = image.arrayWidth() / 8;
        int yPortion = image.arrayHeight() / 8;
        Mat regionOfInterest = null;

        switch (direction) {
            case RIGHT:
                regionOfInterest = image.adjustROI(yPortion, yPortion * 4, xPortion * 4, xPortion * 5);
                break;
            case LEFT:
                regionOfInterest = image.adjustROI(yPortion, yPortion * 4, xPortion, xPortion + xPortion / 3);
                break;
            case DOWN:
                regionOfInterest = image.adjustROI(yPortion * 4, yPortion * 5, xPortion, xPortion * 4);
                break;
            case UP:
                regionOfInterest = image.adjustROI(0, yPortion, xPortion, xPortion * 4);
                break;
            case DIAGONAL_LEFT_TOP:
                regionOfInterest = image.adjustROI(0, yPortion + yPortion / 3, 0, xPortion + xPortion / 3);
                break;
            case DIAGONAL_LEFT_BOTTOM:
                regionOfInterest = image.adjustROI(yPortion * 4 + yPortion / 4, yPortion * 5 - yPortion / 3, 0,
                        xPortion + xPortion / 3);
                break;
            case DIAGONAL_RIGHT_TOP:
                regionOfInterest = image.adjustROI(0, yPortion + yPortion / 3, xPortion * 4, xPortion * 5 + xPortion
                        / 4);
                break;
            case DIAGONAL_RIGHT_BOTTOM:
                regionOfInterest = image.adjustROI(yPortion * 4 + yPortion / 4, yPortion * 5 - yPortion / 3, xPortion
                        * 4, xPortion * 5 + xPortion / 4);
                break;
            default:
                throw new IllegalArgumentException("ImageComparison.getPortion() has no valid action for direction "
                        + direction.toString());
        }
        return regionOfInterest;
    }

    /**
     * Draw rectangles passed on an image
     *
     * @param img  BufferedImage object where the rectangles will be draw
     * @param rect Rectangle object to be draw
     * @throws IOException
     */
    public static void drawRectanglesOnImage(BufferedImage img, Rectangle rect) throws IOException {

        Graphics2D graph = img.createGraphics();
        graph.setColor(Color.BLUE);

        int positionx = (int) Math.ceil(rect.getX());
        int positiony = (int) Math.ceil(rect.getY());
        int graphWidht = (int) Math.ceil(rect.getWidth());
        int graphHeight = (int) Math.ceil(rect.getHeight());

        graph.fill(new Rectangle(positionx, positiony, graphWidht, graphHeight));

        graph.dispose();

    }

    /**
     * Get array with RGBA value of the pixel in the position passed
     *
     * @param img Buffered image to be analyzed
     * @param x   position of the pixel
     * @param y   position of the pixel
     * @return Array that contains the RGBA values for that pixel
     */
    public static int[] getRGBA(BufferedImage img, int x, int y) {
        int[] color = new int[4];
        color[0] = getColor(img, x, y, RED);
        color[1] = getColor(img, x, y, GREEN);
        color[2] = getColor(img, x, y, BLUE);
        color[3] = getColor(img, x, y, ALPHA);
        return color;
    }

    /**
     * Get the value of the color passed for the pixel in that position
     *
     * @param img   Buffered image to be analyzed
     * @param x     position of the pixel
     * @param y     position of the pixel
     * @param color COLOR which value is wanted for that pixel
     * @return
     */
    private static int getColor(BufferedImage img, int x, int y, int color) {
        int value = img.getRGB(x, y) >> color & 0xff;
        return value;
    }

    /**
     * Delete the file passed as parameter
     *
     * @param filePath path to be deleted
     */
    public static void deleteFile(String filePath) {
        File file = new File(filePath);

        deleteFile(file);
    }

    /**
     * Delete the file passed as parameter
     *
     * @param file File object to delete
     */
    public static void deleteFile(File file) {
        if (file.delete()) {
            log.info(file.getAbsolutePath() + " deleted");
        } else {
            log.info(file.getAbsolutePath() + " could not be deleted");
        }
    }
}