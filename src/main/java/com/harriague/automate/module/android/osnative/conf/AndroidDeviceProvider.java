package com.harriague.automate.module.android.osnative.conf;

import org.apache.tools.ant.util.LinkedHashtable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class AndroidDeviceProvider {
    private static final String ANDROID_HOME = System.getenv("ANDROID_HOME");
    private static final String GET_DEVICES_COMMAND_LINE = ANDROID_HOME + java.io.File.separator + "platform-tools"
            + java.io.File.separator + "adb devices";
    private static AndroidDeviceProvider instance;
    static private LinkedHashtable<String,Boolean> DEVICES = new LinkedHashtable<>();
    private AndroidDeviceProvider() throws IOException {
        setDevices();
    }

    public void setDevices() throws IOException {
        Process p = Runtime.getRuntime().exec(GET_DEVICES_COMMAND_LINE);
        InputStream is = p.getInputStream();
        int chr;
        while((chr = is.read()) != -1 && chr != 10) {
        }
        DEVICES = new LinkedHashtable<>();
        while(chr != -1) {
            StringBuilder sb = new StringBuilder(20);
            while((chr = is.read()) != -1 && chr != 9) {
                if(chr != '\r' && chr != '\n')
                    sb.append((char)chr);
            }
            if(sb.length() != 0)
                DEVICES.put(sb.toString(), false);
        }
    }

    public static AndroidDeviceProvider getInstance() throws IOException {
        if(instance == null) {
            instance = new AndroidDeviceProvider();
        }
        return instance;
    }

    public String getUnusedId(boolean markAsUsed) {
        for (Map.Entry<String, Boolean> device : DEVICES.entrySet()) {
            if (!device.getValue()) {
                if (markAsUsed) {
                    device.setValue(true);
                }
                return device.getKey();
            }
        }
        return null;
    }
}
